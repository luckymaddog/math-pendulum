import javafx.util.Pair;

class MathPendulum {

    private double previousX = 0.0;
    private double previousY = 0.0;
    private double deltaT = 0.1;
    private double coefficient = 1.0;
    double kp = 0.0;
    double ki = 0.0;
    double kd = 0.0;

    MathPendulum(double lenght, double aPreviousY, double aDeltaT) {
        deltaT = aDeltaT;
        previousX = 0.0;
        previousY = aPreviousY;//lenght - Math.cos(aPreviousY)*lenght;
        coefficient = -9.8 / lenght;
    }


    double doStep() {
        //  y=(Math.abs(y)-Math.abs(previousY))/2;

        double x = previousX + deltaT * coefficient * Math.sin(previousY);
        double y = previousY + deltaT * previousX;
//        double correctX = previousX+deltaT*coefficient*Math.sin(y);
//        double correctY =  previousY+deltaT*(previousX+correctX)/2.0;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2.0;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2.0;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2;
//        correctX = previousX+deltaT*coefficient*Math.sin(correctY);
//        correctY = previousY+deltaT*(previousX+correctX)/2;
//        double v = Math.abs(x) - Math.abs(correctX);
//        System.out.println(v);
//        Pair<Double, Double> correct = correct(previousY, previousY, previousX);
//        if (y>Math.PI/2)
//        {
//            y = Math.PI/2;
//        }


        previousY = y;
        previousX = x;

        return previousY;
    }

    void usePID(int asteps) {
        pidfunk = new double[asteps];
        for (int i = 0; i < asteps; i++) {
            pidfunk[i] = doStep();
        }
        double sum = 0.0;
        for (int i = 1; i < asteps; i++) {
            for (int j = 1; j <= i; j++) {
                sum += pidfunk[i] * deltaT;
            }
            pidfunk[i] = kp * pidfunk[i] + ki * sum + kd * (pidfunk[i] - pidfunk[i - 1]) / deltaT;
            sum = 0.0;
        }
    }

    double[] pidfunk = new double[0];

    private Pair<Double, Double> correct(double aPreviousY, double aCurrentY, double aPreviousX) {
        double correctX = aPreviousX + deltaT * coefficient * Math.sin(aCurrentY);
        double y = aPreviousY + deltaT * (aPreviousX + correctX) / 2;
        double x = previousX;
//        System.out.println(Math.abs(Math.abs(x)-Math.abs(correctX)));

        if (Math.abs(Math.abs(y) - Math.abs(aCurrentY)) > 0.5) {
            System.out.println("correct");
            Pair<Double, Double> correct = correct(aCurrentY, y, correctX);
            y = correct.getValue();
            x = correct.getKey();
        }
        return new Pair<>(x, y);
    }

    void setLenght(int aLenght) {
        coefficient = -9.8 / aLenght;
    }

    void setStart(double aStart) {
        previousX = 0.0;
        previousY = aStart;
    }
}
